package com.artivisi.training.microservice201902.frontend.dto;

import lombok.Data;

@Data
public class Customer {
    private String id;
    private String name;
    private String number;
    private String email;
    private String mobilePhone;
}
