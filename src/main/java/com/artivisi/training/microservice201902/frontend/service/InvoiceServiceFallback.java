package com.artivisi.training.microservice201902.frontend.service;

import com.artivisi.training.microservice201902.frontend.dto.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class InvoiceServiceFallback implements InvoiceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceServiceFallback.class);

    @Override
    public Iterable<Customer> ambilDataCustomer() {
        LOGGER.info("Menjalankan fallback ambil data customer");
        return new ArrayList<>();
    }
}
