package com.artivisi.training.microservice201902.frontend.service;

import com.artivisi.training.microservice201902.frontend.dto.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "invoice", fallback = InvoiceServiceFallback.class)
public interface InvoiceService {

    @GetMapping("/customer/")
    public Iterable<Customer> ambilDataCustomer();
}
