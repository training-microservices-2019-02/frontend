package com.artivisi.training.microservice201902.frontend.controller;

import com.artivisi.training.microservice201902.frontend.dto.NotificationRequest;
import com.artivisi.training.microservice201902.frontend.service.InvoiceService;
import com.artivisi.training.microservice201902.frontend.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CustomerController {

    @Autowired private InvoiceService invoiceService;
    @Autowired private KafkaProducerService kafkaProducerService;

    @GetMapping("/customer/list")
    public ModelMap dataCustomer() {
        return new ModelMap().addAttribute("dataCustomer", invoiceService.ambilDataCustomer());
    }

    @GetMapping("/customer/notifikasi")
    public void tampilkanFormNotifikasi() {

    }

    @PostMapping("/customer/notifikasi")
    public String prosesFormNotifikasi(@ModelAttribute NotificationRequest req) {
        kafkaProducerService.kirimNotifikasi(req);
        return "redirect:notifikasi";
    }
}
